import React from "react";
import { Link } from "react-router-dom";
import logo from "./../../assets/image/logo.svg";

const Sidebar = ({ showSidebar, setShowSidebar }) => {
  return (
    <>
      <div
        onClick={() => setShowSidebar(false)}
        className={
          showSidebar
            ? "bg-black bg-opacity-20 cursor-pointer w-full h-screen  fixed left-0 top-0 z-[800]"
            : "bg-black bg-opacity-20 cursor-pointer w-full h-screen  fixed -left-full top-0 z-[800]"
        }
      ></div>
      <div
        className={
          showSidebar
            ? "bg-white border-r-2 border-primary shadow-lg w-[300px] h-screen overflow-x-auto fixed left-0 transition-all duration-500 top-0 z-[1000]"
            : "bg-white border-r-2 border-primary shadow-lg w-[300px] h-screen overflow-x-auto fixed -left-full transition-all duration-500 top-0 z-[1000]"
        }
      >
        <div className="w-full py-4 px-3 border-b flex justify-between items-center">
          <div>
            <Link to={"/"}>
              <img className="w-32 cursor-pointer" src={logo} alt="" />
            </Link>
          </div>
          <div
            onClick={() => setShowSidebar(false)}
            className="w-7 h-7 flex justify-center items-center bg-dark text-white cursor-pointer"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <line x1="18" y1="6" x2="6" y2="18"></line>
              <line x1="6" y1="6" x2="18" y2="18"></line>
            </svg>
          </div>
        </div>
        <div className="mt-10">
          <ul>
            <li className="">
              <Link
                to={"/"}
                className="w-full px-4 py-4 border-l-4 border-l-transparent hover:border-l-primary transition-all duration-300 inline-block border-b"
              >
                Home
              </Link>
            </li>

            <li className="">
              <Link
                to={"/"}
                className="w-full px-4 py-4 border-l-4 border-l-transparent hover:border-l-primary transition-all duration-300 inline-block border-b"
              >
                Properties
              </Link>
            </li>
            <li className="">
              <Link
                to={"/"}
                className="w-full px-4 py-4 border-l-4 border-l-transparent hover:border-l-primary transition-all duration-300 inline-block border-b"
              >
                Agents
              </Link>
            </li>
            <li className="">
              <Link
                to={"/"}
                className="w-full px-4 py-4 border-l-4 border-l-transparent hover:border-l-primary transition-all duration-300 inline-block border-b"
              >
                Blog
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default Sidebar;
