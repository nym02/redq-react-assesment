import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import ApartmentInfo from "./ApartmentInfo";
import areaIcon from "./../../assets/image/area.svg";
import bedIcon from "./../../assets/image/bed.svg";
import bathIcon from "./../../assets/image/bath.svg";

const ApartmentDetailsCard = ({
  title,
  price,
  discountedPrice,
  area,
  room,
  bath,
  isForRent,
}) => {
  return (
    <div className="max-w-md sm:max-w-full sm:mx-0 mx-auto bg-white shadow-card rounded-md overflow-hidden mb-4 sm:mb-0">
      {/* slider part  */}
      <div className=" overflow-hidden relative">
        {/* wishlist button  */}
        <div className="absolute right-2 top-2 z-50 cursor-pointer">
          <svg
            width="32"
            height="32"
            viewBox="0 0 32 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect
              width="32"
              height="32"
              rx="8"
              fill="black"
              fillOpacity="0.3"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M7.25 14.0298C7.25 11.3293 9.61914 9.25 12.4 9.25C13.8335 9.25 15.0948 9.92214 16 10.7918C16.9052 9.92214 18.1665 9.25 19.6 9.25C22.3809 9.25 24.75 11.3293 24.75 14.0298C24.75 15.8797 23.9611 17.5064 22.8682 18.8815C21.7771 20.2543 20.35 21.4193 18.9835 22.366C18.4615 22.7276 17.9335 23.0611 17.4503 23.3072C16.9965 23.5383 16.4747 23.75 16 23.75C15.5253 23.75 15.0035 23.5383 14.5497 23.3072C14.0665 23.0611 13.5385 22.7276 13.0165 22.366C11.65 21.4193 10.2229 20.2543 9.13182 18.8815C8.03888 17.5064 7.25 15.8797 7.25 14.0298ZM12.4 10.75C10.3208 10.75 8.75 12.2791 8.75 14.0298C8.75 15.4333 9.34579 16.74 10.3061 17.9482C11.2683 19.1588 12.5629 20.2269 13.8707 21.133C14.3656 21.4758 14.8317 21.7675 15.2305 21.9706C15.6586 22.1886 15.9067 22.25 16 22.25C16.0933 22.25 16.3414 22.1886 16.7695 21.9706C17.1683 21.7675 17.6344 21.4758 18.1293 21.133C19.4371 20.2269 20.7317 19.1588 21.6939 17.9482C22.6542 16.74 23.25 15.4333 23.25 14.0298C23.25 12.2791 21.6792 10.75 19.6 10.75C18.4058 10.75 17.2908 11.4634 16.5946 12.3689C16.4526 12.5536 16.2329 12.6618 16 12.6618C15.7671 12.6618 15.5474 12.5536 15.4054 12.3689C14.7092 11.4634 13.5942 10.75 12.4 10.75Z"
              fill="white"
            />
          </svg>
        </div>
        {/* wishlist button  */}
        <OwlCarousel className="owl-theme" loop margin={10} items={1} dots>
          <div class="item">
            <img
              className="w-full h-[256px] object-cover"
              src="https://images.unsplash.com/photo-1515263487990-61b07816b324?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxMTgwOTN8MHwxfHNlYXJjaHwyfHxhcGFydG1lbnR8ZW58MHx8fHwxNjUxMjY0NDIz&ixlib=rb-1.2.1&q=80&w=1080"
              alt=""
            />
          </div>
          <div class="item">
            <img
              className="w-full h-[256px] object-cover"
              src="https://images.unsplash.com/photo-1545324418-cc1a3fa10c00?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxMTgwOTN8MHwxfHNlYXJjaHw2fHxhcGFydG1lbnR8ZW58MHx8fHwxNjUxMjY0NDIz&ixlib=rb-1.2.1&q=80&w=1080"
              alt=""
            />
          </div>
        </OwlCarousel>
      </div>
      {/* slider part  */}
      {/* apartment pricing info  */}
      <div className="py-5 px-4 bg-white">
        <div>
          <h1 className="font-medium text-lg text-dark">
            5300 Lakeside, Newyork
          </h1>
        </div>
        <div className="mt-[18px] flex space-x-4 items-center">
          <div>
            <button className="bg-white border-2 border-primary rounded-md text-primary text-xs font-medium hover:bg-primary hover:text-white transition-all duration-300 px-[14px] py-2">
              {isForRent ? "For Rent" : "For Buy"}
            </button>
          </div>
          <div>
            <h1 className="font-medium font-lg text-primary">
              <span>$</span>
              {new Intl.NumberFormat().format(discountedPrice)}
            </h1>
          </div>
          {isForRent && (
            <>
              <div>
                <h1 className="font-normal font-sm text-[#999999] line-through">
                  <span>$</span>
                  {new Intl.NumberFormat().format(price)}
                </h1>
              </div>
            </>
          )}
        </div>
        {/* apartment specification info  */}
        <div className="mt-[24px] w-full flex space-x-4 items-center">
          <div>
            <ApartmentInfo icon={areaIcon} info={`${area} Sqft`} />
          </div>
          <div>
            <ApartmentInfo icon={bedIcon} info={`${room} Rooms`} />
          </div>
          <div>
            <ApartmentInfo icon={bathIcon} info={`${bath} Bath`} />
          </div>
        </div>
        {/* apartment specification info  */}
      </div>
      {/* apartment pricing info  */}
    </div>
  );
};

export default ApartmentDetailsCard;
