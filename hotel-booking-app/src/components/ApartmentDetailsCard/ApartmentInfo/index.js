import React from "react";

const ApartmentInfo = ({ icon, info }) => {
  return (
    <div className="w-full flex space-x-2 2xl:space-x-1 items-center">
      <div>
        <img src={icon} alt="" />
      </div>
      <div>
        <h1 className="text-sm text-darkGray">{info}</h1>
      </div>
    </div>
  );
};

export default ApartmentInfo;
