import React from "react";

const ProductHeader = ({ title, buttonText }) => {
  return (
    <div className="max-w-[1790px] mx-auto w-11/12 py-4">
      <div className="w-full flex justify-between items-center">
        <div>
          <h1 className="font-bold text-lg md:text-[32px] text-dark">
            {title}
          </h1>
        </div>
        <div>
          <button className="inline-flex text-sm md:text-base space-x-2 items-center text-primary font-medium">
            <span>{buttonText}</span>
            <span>
              <svg
                width="8"
                height="12"
                viewBox="0 0 8 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.01592 0.265916C1.37047 -0.0886387 1.94532 -0.0886387 2.29987 0.265916L7.14198 5.10802C7.49653 5.46258 7.49653 6.03742 7.14198 6.39198L2.29987 11.2341C1.94532 11.5886 1.37047 11.5886 1.01592 11.2341C0.661361 10.8795 0.661361 10.3047 1.01592 9.95013L5.21604 5.75L1.01592 1.54987C0.661361 1.19532 0.661361 0.620471 1.01592 0.265916Z"
                  fill="#007882"
                />
              </svg>
            </span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductHeader;
