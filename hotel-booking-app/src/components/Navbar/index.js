import React from "react";
import { Link } from "react-router-dom";
import logo from "./../../assets/image/logo.svg";
import menuIcon from "./../../assets/image/hamburgerIcon.svg";

const Navbar = ({ showSidebar, setShowSidebar }) => {
  const handleSidebar = () => {
    setShowSidebar(true);
  };
  return (
    <>
      <div className="w-full">
        <div className="max-w-[1790px] mx-auto w-11/12 py-4 lg:py-6">
          <div className="w-full flex justify-between items-center">
            {/* logo  */}
            <div>
              <img className="w-24 md:w-32" src={logo} alt="" loading="lazy" />
            </div>
            {/* logo  */}
            {/* menu  */}
            <div>
              <div className="md:w-full md:flex md:space-x-10 md:items-center">
                <div className="hidden md:block">
                  <ul className="md:w-full md:flex md:items-center md:space-x-8">
                    <li>
                      <Link
                        to="/"
                        className="font-medium hover:text-primary text-dark transition-all duration-300"
                      >
                        Home
                      </Link>
                    </li>
                    <li>
                      <Link
                        to="/"
                        className="font-medium hover:text-primary text-dark transition-all duration-300"
                      >
                        Properties
                      </Link>
                    </li>
                    <li>
                      <Link
                        to="/"
                        className="font-medium hover:text-primary text-dark transition-all duration-300"
                      >
                        Agents
                      </Link>
                    </li>
                    <li>
                      <Link
                        to="/"
                        className="font-medium hover:text-primary text-dark transition-all duration-300"
                      >
                        Blog
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className="flex space-x-4 items-center">
                  <div>
                    <Link
                      to="/login"
                      className="text-sm font-medium text-white bg-primary px-6 md:px-8 shadow-login py-2 rounded-full"
                    >
                      Login
                    </Link>
                  </div>
                  <div>
                    <div
                      onClick={() => handleSidebar()}
                      className="w-10 h-10 bg-white cursor-pointer shadow-menuIcon rounded-full flex justify-center items-center"
                    >
                      <img src={menuIcon} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* menu  */}
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
