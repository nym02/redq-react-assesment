import React, { useState } from "react";
import ApartmentDetailsCard from "../../components/ApartmentDetailsCard";
import Navbar from "../../components/Navbar";
import ProductHeader from "../../components/ProductHeader";
import Sidebar from "../../components/Sidebar";

const Home = () => {
  const [showSidebar, setShowSidebar] = useState(false);
  const ApartmentArr = [
    {
      id: 1,
      title: "5300 Lakeside, Newyork",
      price: 16000,
      discountedPrice: 14000,
      area: 1150,
      room: 4,
      bath: 2,
      isForRent: false,
    },
    {
      id: 2,
      title: "5300 Lakeside, Newyork",
      price: 16000,
      discountedPrice: 14000,
      area: 1150,
      room: 4,
      bath: 2,
      isForRent: true,
    },
    {
      id: 3,
      title: "5300 Lakeside, Newyork",
      price: 16000,
      discountedPrice: 14000,
      area: 1150,
      room: 4,
      bath: 2,
      isForRent: false,
    },
    {
      id: 4,
      title: "5300 Lakeside, Newyork",
      price: 16000,
      discountedPrice: 14000,
      area: 1150,
      room: 4,
      bath: 2,
      isForRent: true,
    },
    {
      id: 5,
      title: "5300 Lakeside, Newyork",
      price: 16000,
      discountedPrice: 14000,
      area: 1150,
      room: 4,
      bath: 2,
      isForRent: false,
    },
  ];
  return (
    <div className="bg-primaryBg w-full h-screen font-jost relative">
      <div>
        <Sidebar showSidebar={showSidebar} setShowSidebar={setShowSidebar} />
      </div>
      <div>
        <Navbar showSidebar={showSidebar} setShowSidebar={setShowSidebar} />
      </div>
      <div className="py-[100px]">
        <div>
          <ProductHeader title="All Apartments" buttonText="Show All" />
        </div>
        <div className="max-w-[1790px] mx-auto w-11/12 py-4">
          <div className="w-full sm:grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 sm:gap-2 md:gap-5 xl:gap-5">
            {ApartmentArr.map((ApartArr) => (
              <>
                <ApartmentDetailsCard
                  key={ApartArr?.id}
                  title={ApartArr?.title}
                  price={ApartArr?.price}
                  discountedPrice={ApartArr?.discountedPrice}
                  area={ApartArr?.area}
                  room={ApartArr?.room}
                  bath={ApartArr?.bath}
                  isForRent={ApartArr?.isForRent}
                />
              </>
            ))}
          </div>
          <div className="bg-lightGray w-full h-[2px] mt-10">
            <div className="bg-primary h-[3px] w-[30%]"></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
