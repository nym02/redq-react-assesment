module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primaryBg: "#FAFAFA",
        primary: "#007882",
        dark: "#110229",
        lightGray: "#CDD1D5",
        darkGray: "#666666",
      },
      fontFamily: {
        jost: "'Jost', sans-serif",
      },
      boxShadow: {
        login:
          "0px 2px 4px rgba(0, 0, 0, 0.06), 0px 4px 6px rgba(0, 0, 0, 0.1)",
        menuIcon: "0px 2px 8px rgba(0, 0, 0, 0.06)",
        card: "0px 2px 4px rgba(0, 0, 0, 0.08)",
      },
    },
  },
  plugins: [],
};
